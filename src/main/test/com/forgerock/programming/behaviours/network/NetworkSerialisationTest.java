/*
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.network;

import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Snake;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Responsible for
 *
 * @author robert.wapshott@forgerock.com
 */
public class NetworkSerialisationTest {
    @Test
    public void shouldDeserialiseSerialisedResult() {
        // Given
        List<Entity> entities = new ArrayList<Entity>();
        Snake test = new Snake();
        test.setX(100);
        test.setY(50);
        entities.add(test);

        NetworkSerialisation serialisation = new NetworkSerialisation(new ClassCharacterMap());

        // When
        List<Entity> result = serialisation.deserialise(serialisation.serialise(entities));

        // Then
        assertEquals(1, result.size());
        Entity e = result.get(0);
        assertEquals(test.getX(), e.getX());
        assertEquals(test.getY(), e.getY());
        assertEquals(Snake.class, e.getClass());
    }
}
