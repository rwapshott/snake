/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming;

import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.behaviours.GameLoopEvents;
import com.forgerock.programming.model.Entity;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Responsible for updating the game state.
 *
 * @author robert.wapshott@forgerock.com
 */
public class GameManager {

    private List<Entity> entities = new ArrayList<Entity>();

    private List<Entity> removals = new ArrayList<Entity>();
    private List<Entity> additions = new ArrayList<Entity>();

    private final Behaviour behaviour;
    private final GameLoopEvents events;

    @Inject
    public GameManager(Behaviour behaviour, GameLoopEvents postUpdate) {
        this.behaviour = behaviour;
        this.events = postUpdate;
    }

    public void addEntity(Entity entity) {
        additions.add(entity);
    }

    public void removeEntity(Entity entity) {
        removals.add(entity);
    }

    /**
     * Update all entities in the game by using all behaviours.
     */
    public void update() {
        // Perform all pre loop events.
        events.preUpdate();

        // Perform the game loop.
        for (Entity entity : entities) {
            behaviour.process(entity);
        }

        // Process additions and removals.
        entities.removeAll(removals);
        removals.clear();

        entities.addAll(additions);
        additions.clear();

        // Perform all post loop events
        events.postUpdate();
    }

    /**
     * Determines if any entities have collided with the entity.
     * @param entity Entity to check if there are any collisions.
     * @return Will not contain the entity provided.
     */
    public List<Entity> getCollisions(Entity entity) {
        List<Entity> collisions = new ArrayList<Entity>();
        for (Entity test : entities) {
            if (haveEntitiesCollided(test, entity)) {
                collisions.add(test);
            }
        }
        return collisions;
    }

    private static boolean haveEntitiesCollided(Entity one, Entity two) {
        if (one.equals(two)) return false;
        if (one.getX() == two.getX() && one.getY() == two.getY()) {
            return true;
        }
        return false;
    }

    /**
     * Provides a view of the entities.
     * @return A unmodifiable collection.
     */
    public Collection<Entity> getEntities() {
        return Collections.unmodifiableCollection(entities);
    }
}
