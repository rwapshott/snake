/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming;

/**
 * Responsible for tracking global game constants.
 *
 * @author robert.wapshott@forgerock.com
 */
public class GameConstants {
    public static final int GAME_WIDTH = 80;
    public static final int GAME_HEIGHT = 20;
    public static final int UPDATE_TIMER = 50;
    public static final int SNAKE_MOVE_SPEED = 5;
    public static final int UDP_PORT = 48371;
}
