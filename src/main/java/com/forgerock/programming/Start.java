/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming;

import com.forgerock.programming.behaviours.Grid;
import com.forgerock.programming.behaviours.PlayerControl;
import com.forgerock.programming.behaviours.network.udp.NetworkReceiver;
import com.forgerock.programming.behaviours.network.udp.NetworkSender;
import com.forgerock.programming.behaviours.snake.SnakeCollision;
import com.forgerock.programming.behaviours.snake.SnakeEat;
import com.forgerock.programming.behaviours.snake.SnakeMovement;
import com.forgerock.programming.behaviours.tail.TailFollow;
import com.forgerock.programming.guice.GameModule;
import com.forgerock.programming.swing.Frame;
import com.forgerock.programming.swing.GridDisplay;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Responsible for launching the application.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Start {
    private static Injector injector;

    public static <T> T getInstance(Class<T> c) {
        if (injector == null) {
            injector = Guice.createInjector(new GameModule());
        }
        return injector.getInstance(c);
    }

    @Inject
    private Start(Grid grid, final GridDisplay gridDisplay, GameFactory factory, Frame frame,
                  final GameManager manager, SnakeMovement snakeMovement, SnakeCollision snakeCollision,
                  SnakeEat snakeEat, TailFollow tailFollow, PlayerControl control,
                  final NetworkSender sender, final NetworkReceiver receiver) {

        // Create Render
        frame.showDisplay();

        // Populate game world
        for (int ii = 0; ii < 5; ii++) {
            manager.addEntity(factory.makeApple());
        }

        // Make a snake
        manager.addEntity(factory.makeSnake());

        // Create a game loop
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    // Reset those which need to be reset.
//                    receiver.update();
                    manager.update();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },0, GameConstants.UPDATE_TIMER, TimeUnit.MILLISECONDS);
    }

    public static void main(String[] args) {
        getInstance(Start.class);
    }
}
