/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.swing;


import com.forgerock.programming.Start;
import com.forgerock.programming.swing.actions.CloseAction;
import com.google.inject.Inject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Responsible for managing the details of creating and displaying the JFrame.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Frame extends JFrame {

    private GridDisplay display;

    @Inject
    public Frame(GridDisplay display) {
        this.display = display;
    }

    public void showDisplay() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Display
                setLayout(new BorderLayout());
                add(display, BorderLayout.CENTER);

                // Actions
                final Action close = Start.getInstance(CloseAction.class);

                // Frame Management
                setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        close.actionPerformed(null);
                    }
                });

                pack();
                setResizable(false);
                setLocationRelativeTo(null);
                setVisible(true);
            }
        });
    }
}
