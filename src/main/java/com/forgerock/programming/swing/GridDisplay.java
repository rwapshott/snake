/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.swing;

import com.forgerock.programming.GameConstants;
import com.forgerock.programming.behaviours.GameLoopEvents;
import com.forgerock.programming.behaviours.Grid;
import com.google.inject.Inject;

import javax.swing.*;
import java.awt.*;

/**
 * Responsible for providing a rendering surface for the game.
 *
 * @author robert.wapshott@forgerock.com
 */
public class GridDisplay extends JComponent {
    private static final int CELL_SIZE = 12;
    private static final Font MONOSPACED = new Font("Monospaced", Font.PLAIN, CELL_SIZE);
    private Grid grid;

    @Inject
    public GridDisplay(Grid grid, GameLoopEvents events) {
        this.grid = grid;
        setPreferredSize(new Dimension(GameConstants.GAME_WIDTH * CELL_SIZE + CELL_SIZE, GameConstants.GAME_HEIGHT * CELL_SIZE + CELL_SIZE));

        events.registerPostUpdate(new GameLoopEvents.PostUpdateListener() {
            @Override
            public void postUpdate() {
                repaint();
            }
        });
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setFont(MONOSPACED);
        for (int y = 0; y < GameConstants.GAME_HEIGHT; y++) {
            for (int x = 0; x < GameConstants.GAME_WIDTH; x++) {
                String c = Character.toString(grid.get(x, y));
                int drawX = x * CELL_SIZE + CELL_SIZE;
                int drawY = y * CELL_SIZE + CELL_SIZE;
                g.drawString(c, drawX, drawY);
            }
        }
        invalidate();
    }
}
