/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.model;

import com.forgerock.programming.GameConstants;

/**
 * Responsible for modelling a snake.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Snake extends Entity {
    private Direction direction = Direction.RIGHT;
    private int moveTimer = GameConstants.SNAKE_MOVE_SPEED;
    private Tail tail;

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getTailLength() {
        int count = 0;
        Tail t = getTail();
        while (t != null) {
            count++;
            t = t.getTail();
        }
        return count;
    }

    public Tail getTailEnd() {
        if (tail == null) return null;
        Tail t = getTail();
        Tail last = t;
        while (t != null) {
            last = t;
            t = t.getTail();
        }
        return last;
    }

    public boolean isThisYourTail(Tail yours) {
        Tail t = tail;
        while (t != null) {
            if (t.equals(yours)) {
                return true;
            }
            t = t.getTail();
        }
        return false;
    }

    public Tail getTail() {
        return tail;
    }

    public void setTail(Tail tail) {
        this.tail = tail;
    }

    public int getMoveTimer() {
        return moveTimer;
    }

    public void setMoveTimer(int moveTimer) {
        this.moveTimer = moveTimer;
    }
}
