/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.model;

/**
 * Models a tail which will follow the Entity it is pointed to.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Tail extends Entity {
    private Entity head;
    private Tail tail;

    private int[] lastHeadPosition;

    public Entity getHead() {
        return head;
    }

    public void setHead(Entity head) {
        this.head = head;
        lastHeadPosition = new int[]{head.getX(), head.getY()};

        if (head instanceof Tail) {
            Tail tHead = (Tail) head;
            tHead.setTail(this);
        } else if (head instanceof Snake) {
            Snake s = (Snake) head;
            s.setTail(this);
        }
    }

    public Tail getTail() {
        return tail;
    }

    public void setTail(Tail tail) {
        this.tail = tail;
    }

    public int[] getLastHeadPosition() {
        return lastHeadPosition;
    }

    public void setLastHeadPosition(int[] lastHeadPosition) {
        this.lastHeadPosition = lastHeadPosition;
    }
}
