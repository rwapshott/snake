/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.model;

/**
 * Responsible for describing the directions of movement in the game.
 *
 * @author robert.wapshott@forgerock.com
 */
public enum Direction {
    UP(0),
    RIGHT(1),
    DOWN(2),
    LEFT(3);

    private final int index;
    Direction(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public static boolean isDirectionPerpendicular(Direction current, Direction target) {
        int index = current.getIndex();
        index++;
        if (index > 3) {
            index = 0;
        }
        if (target.getIndex() == index) {
            return true;
        }
        index = current.getIndex();
        index--;
        if (index < 0) {
            index = 3;
        }
        if (target.getIndex() == index) {
            return true;
        }
        return false;
    }
}
