/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.tail;

import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Tail;
import com.google.inject.Inject;

/**
 * Responsible for moving tails.
 *
 * @author robert.wapshott@forgerock.com
 */
public class TailFollow {
    @Inject
    public TailFollow(Behaviour behaviour) {
        behaviour.registerEntityType(Tail.class, new Behaviour.BehaviourListener<Tail>() {
            @Override
            public void process(Tail tail) {
                if (shouldTailMove(tail)) {
                    moveTail(tail);
                }
            }
        });
    }

    private void moveTail(Tail tail) {
        int[] position = tail.getLastHeadPosition();
        tail.setX(position[0]);
        tail.setY(position[1]);
        Entity head = tail.getHead();
        tail.setLastHeadPosition(new int[]{head.getX(), head.getY()});
    }

    private boolean shouldTailMove(Tail tail) {
        Entity head = tail.getHead();
        int[] last = tail.getLastHeadPosition();
        return last[0] != head.getX() || last[1] != head.getY();
    }
}
