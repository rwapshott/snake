/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Responsible for providing a mechanism for behaviours to be notified
 * about game loop events.
 *
 * The game loop will iterate over all game objects and update their
 * state. During this various behaviours may wish to be notified. This
 * class provides a mechanism for this.
 *
 * @author robert.wapshott@forgerock.com
 */
public class GameLoopEvents {
    private Collection<PostUpdateListener> post = new ArrayList<PostUpdateListener>();
    private Collection<PreUpdateListener> pre = new ArrayList<PreUpdateListener>();


    /**
     * @param listener The listener to be registered for interest.
     */
    public void registerPostUpdate(PostUpdateListener listener) {
       post.add(listener);
    }

    /**
     * @param listener The listener to be registered.
     */
    public void registerPreUpdate(PreUpdateListener listener) {
        pre.add(listener);
    }

    /**
     * Called when the game loop has reached the end of the loop.
     */
    public void postUpdate() {
        for (PostUpdateListener listener : post) {
            listener.postUpdate();
        }
    }

    /**
     * Called before the game loop starts.
     */
    public void preUpdate() {
        for (PreUpdateListener listener : pre) {
            listener.preUpdate();
        }
    }

    /**
     * Implementations will receive a notification when the game loop
     * reaches the end of its loop.
     */
    public static interface PostUpdateListener {
        void postUpdate();
    }

    /**
     * Implementations will receive a notification before the game loop
     * starts processing.
     */
    public static interface PreUpdateListener {
        void preUpdate();
    }
}
