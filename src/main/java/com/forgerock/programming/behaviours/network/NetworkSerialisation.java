/*
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.network;

import com.forgerock.programming.Start;
import com.forgerock.programming.model.Entity;
import com.google.inject.Inject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Responsible for serialising and deserialising entities into a binary form.
 *
 * This binary format is compact and ideal for network UDP networking or storing to a file.
 *
 * @author robert.wapshott@forgerock.com
 */
public class NetworkSerialisation {
    // Injected
    private ClassCharacterMap map;

    private ByteArrayOutputStream bout = new ByteArrayOutputStream();
    private DataOutputStream dout = new DataOutputStream(bout);

    @Inject
    public NetworkSerialisation(ClassCharacterMap map) {
        this.map = map;
    }

    public byte[] serialise(Collection<Entity> entities) {
        bout.reset();

        try {

            dout.writeInt(entities.size());
            for (Entity entity : entities) {
                dout.writeInt(entity.getX());
                dout.writeInt(entity.getY());
                dout.writeChar(map.getChar(entity.getClass()));
            }
            dout.flush();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return bout.toByteArray();
    }

    public List<Entity> deserialise(byte[] data) {
        List<Entity> r = new ArrayList<Entity>();
        DataInputStream din = new DataInputStream(new ByteArrayInputStream(data));
        try {

            int count = din.readInt();
            for (int ii = 0; ii < count; ii++) {
                int xpos = din.readInt();
                int ypos = din.readInt();
                char c = din.readChar();

                // Now create the entity
                Class entityClass = map.getClass(c);
                Entity entity = (Entity) Start.getInstance(entityClass);
                entity.setX(xpos);
                entity.setY(ypos);
                r.add(entity);
            }

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return r;
    }
}
