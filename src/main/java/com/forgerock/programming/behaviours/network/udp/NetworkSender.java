/*
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.network.udp;

import com.forgerock.programming.GameConstants;
import com.forgerock.programming.behaviours.GameLoopEvents;
import com.forgerock.programming.behaviours.network.GameStateBroadcaster;
import com.forgerock.programming.behaviours.network.NetworkSerialisation;
import com.forgerock.programming.model.Entity;
import com.google.inject.Inject;
import uk.co.gencoreoperative.udp.broadcast.Broadcast;

import java.util.Collection;

/**
 * Responsible for broadcasting the network state from other clients.
 *
 * @author robert.wapshott@forgerock.com
 */
public class NetworkSender {
    // Injected
    private Broadcast broadcast;
    private NetworkSerialisation serialisation;


    @Inject
    public NetworkSender(final Broadcast broadcast, NetworkSerialisation serialisation,
                         GameLoopEvents events, final GameStateBroadcaster broadcaster) {

        this.broadcast = broadcast;
        this.serialisation = serialisation;

        events.registerPostUpdate(new GameLoopEvents.PostUpdateListener() {
            @Override
            public void postUpdate() {
                send(broadcaster.getEntitiesToBroadcast());
            }
        });
    }

    /**
     * @param entities Entities to serialise and broadcast.
     */
    public void send(Collection<Entity> entities) {
        // Get data from bout.
        byte[] bytes = serialisation.serialise(entities);

        // Send the broadcast
        broadcast.send(GameConstants.UDP_PORT, bytes);
    }

}
