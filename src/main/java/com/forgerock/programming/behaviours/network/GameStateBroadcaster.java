/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.network;

import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.behaviours.GameLoopEvents;
import com.forgerock.programming.model.Apple;
import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.model.Tail;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Responsible for collecting the game state and broadcasting it at the end of an update.
 *
 * @author robert.wapshott@forgerock.com
 */
public class GameStateBroadcaster {
    private Collection<Entity> entities = new ArrayList<Entity>();

    @Inject
    public GameStateBroadcaster(Behaviour behaviour, GameLoopEvents events) {
        behaviour.registerEntityType(Apple.class, new Behaviour.BehaviourListener() {
            @Override
            public void process(Entity entity) {
                appendEntity(entity);
            }
        });
        behaviour.registerEntityType(Snake.class, new Behaviour.BehaviourListener() {
            @Override
            public void process(Entity entity) {
                appendEntity(entity);
            }
        });
        behaviour.registerEntityType(Tail.class, new Behaviour.BehaviourListener() {
            @Override
            public void process(Entity entity) {
                appendEntity(entity);
            }
        });

        events.registerPreUpdate(new GameLoopEvents.PreUpdateListener() {
            @Override
            public void preUpdate() {
                reset();
            }
        });
    }

    private void appendEntity(Entity entity) {
        // Ignore all remote entities.
        if (entity.isRemote()) {
            return;
        }

        entities.add(entity);
    }

    private void reset() {
        entities.clear();
    }

    /**
     * @return A collection of all entities which should be broadcast.
     */
    public Collection<Entity> getEntitiesToBroadcast() {
        return Collections.unmodifiableCollection(entities);
    }
}
