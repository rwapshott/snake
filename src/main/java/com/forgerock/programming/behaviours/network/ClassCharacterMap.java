/*
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.network;

import com.forgerock.programming.model.Apple;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.model.Tail;

import java.util.HashMap;
import java.util.Map;

/**
 * Responsible for providing utility functions for the Network process.
 *
 * @author robert.wapshott@forgerock.com
 */
public class ClassCharacterMap {
    public static final char APPLE_CHAR = 'a';
    public static final char SNAKE_CHAR = 'S';
    public static final char SNAKE_TAIL_CHAR = 's';

    private Map<Character, Class> characterMap = new HashMap<Character, Class>();
    private Map<Class, Character> classMap = new HashMap<Class, Character>();

    public ClassCharacterMap() {
        characterMap.put(SNAKE_CHAR, Snake.class);
        classMap.put(Snake.class, SNAKE_CHAR);

        characterMap.put(APPLE_CHAR, Apple.class);
        classMap.put(Apple.class, APPLE_CHAR);

        characterMap.put(SNAKE_TAIL_CHAR, Tail.class);
        classMap.put(Tail.class, SNAKE_TAIL_CHAR);
    }

    public Class getClass(char c) {
        return characterMap.get(c);
    }

    public char getChar(Class c) {
        return classMap.get(c);
    }

}
