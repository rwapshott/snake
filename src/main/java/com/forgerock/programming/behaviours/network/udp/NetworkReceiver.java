/*
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.network.udp;

import com.forgerock.programming.GameConstants;
import com.forgerock.programming.GameManager;
import com.forgerock.programming.behaviours.network.NetworkSerialisation;
import com.forgerock.programming.model.Entity;
import com.google.inject.Inject;
import uk.co.gencoreoperative.udp.receive.Listen;
import uk.co.gencoreoperative.udp.receive.PacketReceived;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Responsible for receiving packets from the network.
 *
 * @author robert.wapshott@forgerock.com
 */
public class NetworkReceiver {
    // Injected
    private final GameManager manager;
    private final Set<InetAddress> hostsMap;
    private final NetworkSerialisation serialisation;

    private List<byte[]> buffer = new ArrayList<byte[]>();

    @Inject
    public NetworkReceiver(GameManager manager, NetworkSerialisation serialisation) {
        this.manager = manager;
        this.serialisation = serialisation;

        hostsMap = new HashSet<InetAddress>();
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            for (InetAddress address : InetAddress.getAllByName(localHost.getHostName())) {
                hostsMap.add(address);
            }
        } catch (UnknownHostException e) {
            throw new IllegalStateException(e);
        }

        new Listen(new PacketReceived() {
            @Override
            public void received(byte[] data, InetAddress address, int port) {
                if (hostsMap.contains(address)) {
                    return;
                }
                update(data);
            }
        }).onPort(GameConstants.UDP_PORT).start();
    }

    private synchronized void update(byte[] data) {
        buffer.clear();
        buffer.add(data);
    }

    public void update() {
        if (buffer.isEmpty()) {
            return;
        }

        List<Entity> removes = new ArrayList<Entity>();
        for (Entity entity : manager.getEntities()) {
            if (entity.isRemote()) {
                removes.add(entity);
            }
        }

        for (Entity remove : removes) {
            manager.removeEntity(remove);
        }

        List<Entity> adds = serialisation.deserialise(buffer.get(0));
        for (Entity add : adds) {
            manager.addEntity(add);
        }
    }
}
