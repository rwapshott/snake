/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.snake;

import com.forgerock.programming.GameFactory;
import com.forgerock.programming.GameManager;
import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.model.Apple;
import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.model.Tail;
import com.google.inject.Inject;

import java.util.List;

/**
 * Responsible for modelling a Snake colliding with any non Apple.
 *
 * @author robert.wapshott@forgerock.com
 */
public class SnakeCollision {
    private final GameManager manager;
    private final GameFactory factory;

    @Inject
    public SnakeCollision(final GameManager manager, final Behaviour behaviour, GameFactory factory) {
        this.manager = manager;
        this.factory = factory;
        behaviour.registerEntityType(Snake.class, new Behaviour.BehaviourListener<Snake>() {
            @Override
            public void process(Snake snake) {
                checkCollisions(snake);
            }
        });
    }

    private void checkCollisions(Snake snake) {
        List<Entity> collisions = manager.getCollisions(snake);
        boolean collided = false;
        for (Entity collision : collisions) {
            // Skip apples
            if (collision instanceof Apple) {
                continue;
            }

            if (collision instanceof Tail) {
                Tail t = (Tail) collision;

                // Is this the end of the snakes tail?
                if (snake.getTailEnd().equals(t)) {
                    continue;
                }

            }

            collided = true;
            break;
        }

        if (collided) {
            // Remove the snake and tail
            manager.removeEntity(snake);
            Tail t = snake.getTail();
            while (t != null) {
                manager.removeEntity(t);
                t = t.getTail();
            }

            // Add a new snake
            manager.addEntity(factory.makeSnake());
        }
    }
}
