/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.snake;

import com.forgerock.programming.GameConstants;
import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.model.Direction;
import com.forgerock.programming.model.Snake;
import com.google.inject.Inject;

/**
 * Responsible for handling Snake Movement.
 *
 * @author robert.wapshott@forgerock.com
 */
public class SnakeMovement {

    @Inject
    public SnakeMovement(Behaviour behaviour) {
        behaviour.registerEntityType(Snake.class, new Behaviour.BehaviourListener<Snake>() {
            @Override
            public void process(Snake snake) {
                if (shouldMove(snake)) {
                    moveSnake(snake);
                }
            }
        });
    }

    private boolean shouldMove(Snake snake) {
        snake.setMoveTimer(snake.getMoveTimer()-1);
        return snake.getMoveTimer() == 0;
    }

    public void moveSnake(Snake snake) {
        int[] nextPosition = getNextPosition(snake);
        snake.setX(nextPosition[0]);
        snake.setY(nextPosition[1]);

        snake.setMoveTimer(GameConstants.SNAKE_MOVE_SPEED);
    }

    private static int[] getNextPosition(Snake snake) {
        Direction d = snake.getDirection();
        int x = snake.getX();
        int y = snake.getY();
        switch (d) {
            case UP:
                y--;
                if (y < 0) {
                    y = GameConstants.GAME_HEIGHT - 1;
                }
                break;
            case DOWN:
                y++;
                if (y >= GameConstants.GAME_HEIGHT) {
                    y = 0;
                }
                break;
            case LEFT:
                x--;
                if (x < 0) {
                    x = GameConstants.GAME_WIDTH - 1;
                }
                break;
            case RIGHT:
                x++;
                if (x >= GameConstants.GAME_WIDTH) {
                    x = 0;
                }
                break;
            default:
                throw new IllegalStateException();
        }
        return new int[]{x, y};
    }
}
