/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours.snake;

import com.forgerock.programming.GameFactory;
import com.forgerock.programming.GameManager;
import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.model.Apple;
import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.model.Tail;
import com.google.inject.Inject;

/**
 * Responsible for applying the apple eating logic for the game.
 *
 * @author robert.wapshott@forgerock.com
 */
public class SnakeEat {
    private GameManager manager;
    private GameFactory factory;

    @Inject
    public SnakeEat(GameManager manager, GameFactory factory, Behaviour behaviour) {
        this.manager = manager;
        this.factory = factory;
        behaviour.registerEntityType(Snake.class, new Behaviour.BehaviourListener<Snake>() {
            @Override
            public void process(Snake snake) {
                check(snake);
            }
        });
    }

    private void check(Snake snake) {
        for (Entity collided : manager.getCollisions(snake)) {
            if (collided instanceof Apple) {
                Apple apple = (Apple) collided;
                if (apple.isEaten()) continue;

                apple.setEaten(true);
                growSnake(snake);

                manager.removeEntity(apple);
                manager.addEntity(factory.makeApple());
            }
        }
    }

    private void growSnake(Snake snake) {
        Tail last = snake.getTailEnd();
        Entity entity;
        if (last != null) {
            entity = factory.makeTail(last);
        } else {
            entity = factory.makeTail(snake);
        }
        manager.addEntity(entity);
    }
}
