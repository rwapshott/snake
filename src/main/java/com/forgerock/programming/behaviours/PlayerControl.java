/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours;

import com.forgerock.programming.model.Direction;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.swing.GridDisplay;
import com.google.inject.Inject;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Responsible for associating player input to the Snakes direction.
 *
 * @author robert.wapshott@forgerock.com
 */
public class PlayerControl {
    private Direction direction = Direction.RIGHT;

    @Inject
    public PlayerControl(final GridDisplay display, final Behaviour behaviour) {
        // Register keyboard controls
        final KeyAdapter adapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_UP:
                        direction = Direction.UP;
                        break;
                    case KeyEvent.VK_RIGHT:
                        direction = Direction.RIGHT;
                        break;
                    case KeyEvent.VK_DOWN:
                        direction = Direction.DOWN;
                        break;
                    case KeyEvent.VK_LEFT:
                        direction = Direction.LEFT;
                        break;
                }
            }
        };
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                display.addKeyListener(adapter);
                // Ensure that the component gets focus for keyboard events.
                display.setFocusable(true);
                display.requestFocusInWindow();
            }
        });

        // Register behaviour
        behaviour.registerEntityType(Snake.class, new Behaviour.BehaviourListener<Snake>() {
            @Override
            public void process(Snake snake) {
                if (Direction.isDirectionPerpendicular(snake.getDirection(), direction)) {
                    snake.setDirection(direction);
                }
            }
        });
    }
}
