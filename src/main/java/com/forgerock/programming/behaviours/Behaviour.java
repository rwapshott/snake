/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours;

import com.forgerock.programming.model.Entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Responsible for providing a way for game behaviour logic to register an interest
 * in the different types of game entities.
 *
 * When a matching game entity is being updated by the game loop, the behaviour that
 * registered interest will be notified so it can perform its logic if appropriate.
 *
 * The intention is that this design will be more tolerant to changes in the
 * game model.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Behaviour {
    private Map<Class, List<BehaviourListener>> map = new HashMap<Class, List<BehaviourListener>>();

    /**
     * @param tClass The type of Game Entity to register interest in.
     * @param listener The listener to call when processing that entity type.
     * @param <T> The class provided must be a subtype of Entity.
     */
    public <T extends Entity> void registerEntityType(Class<T> tClass, BehaviourListener listener) {
        if (!map.containsKey(tClass)) {
            map.put(tClass, new ArrayList<BehaviourListener>());
        }
        map.get(tClass).add(listener);
    }

    /**
     * @param entity The entity to process.
     */
    public void process(Entity entity) {
        for (Class c : map.keySet()) {
            if (entity.getClass().isAssignableFrom(c)) {
                for (BehaviourListener listener : map.get(c)) {
                    listener.process(entity);
                }
            }
        }
    }

    /**
     * BehaviourListener will be called when an Entity of the registered type is to be processed.
     * @param <T> Type of the entity.
     */
    public static interface BehaviourListener<T extends Entity> {
        void process(T t);
    }
}
