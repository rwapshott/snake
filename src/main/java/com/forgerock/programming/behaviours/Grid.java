/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.behaviours;

import com.forgerock.programming.GameConstants;
import com.forgerock.programming.behaviours.network.ClassCharacterMap;
import com.forgerock.programming.model.Apple;
import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.model.Tail;
import com.google.inject.Inject;

import java.util.Arrays;

/**
 * Responsible for rendering the game objects into text form.
 *
 * The Grid will be GAME_HEIGHT rows tall and GAME_WIDTH columns wide.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Grid {
    char[][] buf = generateBlank();

    @Inject
    public Grid(Behaviour behaviour, GameLoopEvents events) {
        behaviour.registerEntityType(Snake.class, new Behaviour.BehaviourListener<Snake>() {
            @Override
            public void process(Snake snake) {
                update(ClassCharacterMap.SNAKE_CHAR, snake);
            }
        });
        behaviour.registerEntityType(Tail.class, new Behaviour.BehaviourListener<Tail>() {
            @Override
            public void process(Tail snakeTail) {
                update(ClassCharacterMap.SNAKE_TAIL_CHAR, snakeTail);
            }
        });
        behaviour.registerEntityType(Apple.class, new Behaviour.BehaviourListener<Apple>() {
            @Override
            public void process(Apple apple) {
                update(ClassCharacterMap.APPLE_CHAR, apple);
            }
        });

        events.registerPreUpdate(new GameLoopEvents.PreUpdateListener() {
            @Override
            public void preUpdate() {
                reset();
            }
        });
    }

    private void update(char c, Entity entity) {
        buf[entity.getX()][entity.getY()] = c;
    }

    private char[][] generateBlank() {
        char[][] r = new char[GameConstants.GAME_WIDTH][GameConstants.GAME_HEIGHT];
        for (int ii = 0; ii < r.length; ii++) {
            Arrays.fill(r[ii], '.');
        }
        return r;
    }

    private void reset() {
        buf = generateBlank();
    }

    public char get(int x, int y) {
        return buf[x][y];
    }

    public String toString() {
        String r = "";

        for (int y = 0; y < GameConstants.GAME_HEIGHT; y++) {
            for (int x = 0; x < GameConstants.GAME_WIDTH; x++) {
                r += buf[x][y];
            }
            r += "\n";
        }
        return r;
    }
}
