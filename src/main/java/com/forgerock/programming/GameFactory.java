/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming;

import com.forgerock.programming.model.Apple;
import com.forgerock.programming.model.Entity;
import com.forgerock.programming.model.Snake;
import com.forgerock.programming.model.Tail;
import com.google.inject.Inject;

import java.util.Random;

/**
 * Responsible for
 *
 * @author robert.wapshott@forgerock.com
 */
public class GameFactory {

    private Random random;

    @Inject
    public GameFactory(Random random) {
        this.random = random;
    }

    public Entity makeApple() {
        Apple a = Start.getInstance(Apple.class);
        a.setX(random.nextInt(GameConstants.GAME_WIDTH));
        a.setY(random.nextInt(GameConstants.GAME_HEIGHT));
        makeLocal(a);
        return a;
    }

    public Snake makeSnake() {
        Snake s = Start.getInstance(Snake.class);
        s.setX(random.nextInt(GameConstants.GAME_WIDTH));
        s.setY(random.nextInt(GameConstants.GAME_HEIGHT));
        makeLocal(s);
        return s;
    }

    public Tail makeTail(Entity entity) {
        Tail t = Start.getInstance(Tail.class);
        t.setX(entity.getX());
        t.setY(entity.getY());
        t.setHead(entity);
        makeLocal(t);
        return t;
    }

    private void makeLocal(Entity entity) {
        entity.setRemote(false);
    }
}
