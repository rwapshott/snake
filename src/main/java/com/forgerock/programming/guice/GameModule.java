/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.guice;

import com.forgerock.programming.GameManager;
import com.forgerock.programming.behaviours.Behaviour;
import com.forgerock.programming.behaviours.GameLoopEvents;
import com.forgerock.programming.swing.GridDisplay;
import com.forgerock.programming.behaviours.Grid;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

/**
 * Responsible for providing information about game bindings.
 *
 * @author robert.wapshott@forgerock.com
 */
public class GameModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Grid.class).in(Singleton.class);
        bind(GridDisplay.class).in(Singleton.class);
        bind(GameManager.class).in(Singleton.class);
        bind(Behaviour.class).in(Singleton.class);
        bind(GameLoopEvents.class).in(Singleton.class);
    }
}
